# create project
oc login -u developer -p developer
oc new-project gravitee

# add service account for host path
oc create serviceaccount gravitee -n gravitee

# affect policy
oc login -u system:admin
oc project gravitee
oc adm policy add-scc-to-user anyuid -z gravitee
oc adm policy add-scc-to-user hostmount-anyuid -z gravitee -n gravitee

oc login -u developer -p developer

# import OpenShift Template
oc process -f .\gravitee.yaml | oc create -f -

# import Docker Compose
# oc import docker-compose -f .\docker-compose.yml

# expose routes
# oc expose svc/managementapi
# oc expose svc/managementui
# oc expose svc/gateway

