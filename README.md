# README #

Docker file for Grativee.io in OpenShift.

# Import docker compose into Openshift #

oc import docker-compose -f .\docker-compose.yml

# Expose routes into OpenShift #

oc expose svc/managementapi
oc expose svc/managementui
oc expose svc/gateway

# Scale Gateway #

Scale pods of type Gateway:

oc scale dc/gateway --replicas=1

# RAZ environement #

oc delete dc mongodb -n gravitee
oc delete services mongodb -n gravitee
oc delete imagestream mongodb -n gravitee

oc delete dc elasticsearch -n gravitee
oc delete services elasticsearch -n gravitee
oc delete imagestream elasticsearch -n gravitee

oc delete dc managementui -n gravitee
oc delete services managementui -n gravitee
oc delete route managementui -n gravitee
oc delete imagestream managementui -n gravitee

oc delete dc managementapi -n gravitee
oc delete services managementapi -n gravitee
oc delete route managementapi -n gravitee
oc delete imagestream managementapi -n gravitee

oc delete dc gateway -n gravitee
oc delete services gateway -n gravitee
oc delete route gateway -n gravitee
oc delete imagestream gateway -n gravitee

oc delete persistentvolumeclaim elasticdata -n gravitee
oc delete persistentvolumeclaim mongodata -n gravitee
